package umlklassendiagramm.nr.six;

import java.util.Date;

public class User {
    private int nummer;
    private Date geburtsdatum;
    private Date angelegtAm;
    private String vorname;
    private String nachname;
    private boolean istWeiblich;

    public int getNummer() {
        return nummer;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }

    public Date getGeburtsdatum() {
        return geburtsdatum;
    }

    public Date getAngelegtAm() {
        return angelegtAm;
    }

    public String getVorname() {
        return vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public boolean isIstWeiblich() {
        return istWeiblich;
    }

    public void setGeburtsdatum(Date geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    public void setAngelegtAm(Date angelegtAm) {
        this.angelegtAm = angelegtAm;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public void setIstWeiblich(boolean istWeiblich) {
        this.istWeiblich = istWeiblich;
    }

}