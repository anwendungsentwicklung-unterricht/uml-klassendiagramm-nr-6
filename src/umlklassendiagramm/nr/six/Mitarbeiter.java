package umlklassendiagramm.nr.six;

public class Mitarbeiter extends User {

    private String qualifikation;
    private double gehalt;
    private Adresse adresse;

    public void addAdresse(String strasse, String hausnummer, String ort, String plz) {
        adresse = new Adresse(strasse, hausnummer, plz, ort);
    }

    public void setAdresse(Adresse eineAdresse) {

        this.adresse = eineAdresse;

    }

    public void removeAdresse() {
        adresse = null;
    }

    public void setQualifikation(String qualifikation) {
        this.qualifikation = qualifikation;
    }

    public void setGehalt(double gehalt) {
        this.gehalt = gehalt;
    }


    public String getQualifikation() {
        return qualifikation;
    }

    public double getGehalt() {
        return gehalt;
    }


    public Mitarbeiter() {

    }
}