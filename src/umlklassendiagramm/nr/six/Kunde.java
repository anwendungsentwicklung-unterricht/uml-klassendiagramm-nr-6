package umlklassendiagramm.nr.six;

public class Kunde extends User {

    private String telefonnummer;
    private Adresse privateAdresse;
    private Adresse geschaeftlicheAdresse;

    public void addPrivateAdresse(String strasse, String hausnummer, String ort, String plz) {
        privateAdresse = new Adresse(strasse, hausnummer, plz, ort);
    }

    public void setPrivateAdresse(Adresse eineAdresse) {

        this.privateAdresse = eineAdresse;

    }

    public void setGeschaeftlicheAdresse(Adresse eineAdresse) {

        this.geschaeftlicheAdresse = eineAdresse;

    }

    public void removePrivateAdresse() {
        privateAdresse = null;
    }

    public void addGeschaeftlicheAdresse(String strasse, String hausnummer, String ort, String plz) {
        geschaeftlicheAdresse = new Adresse(strasse, hausnummer, plz, ort);
    }

    public void removeGeschaeftliche() {
        geschaeftlicheAdresse = null;
    }

    public String getTelefonnummer() {
        return telefonnummer;
    }

    public void setTelefonnummer(String telefonnummer) {
        this.telefonnummer = telefonnummer;
    }

    public Kunde() {

    }
}